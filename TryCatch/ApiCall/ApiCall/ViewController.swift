//
//  ViewController.swift
//  ApiCall
//
//  Created by Welborn Machado on 17/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var actInd: UIActivityIndicatorView!
    @IBOutlet weak var imgV: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func imgDisplayPress(_ sender: Any) {
        //http://fbdigital.hungama.org/AAD/c1.png
        
        actInd.startAnimating()
        
        let url = URL(string: "http://fbdigital.hungama.org/AAD/c1.png")
        let task = URLSession.shared.dataTask(with: url!) { (data, response, err) in
            
            self.actInd.stopAnimating()

            if (err != nil){
                print(err!)
            }else{
                DispatchQueue.main.async {
                    self.imgV.image = UIImage(data: data!)
                }
            }
           
        }
        
        task.resume()
}
    
    @IBAction func getBasicPress(_ sender: Any) {
        let url = URL(string: "https://mahindralylf.com/apiv1/getholidays")
        let task = URLSession.shared.dataTask(with: url!) { (data, response, err) in
            
            self.actInd.stopAnimating()
            
            if (err != nil){
                print(err!)
            }else{
                do
                {
                let dict = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                
               print(dict)
                    
                    print(dict.value(forKey: "holiday_count")!)
                    
                }catch{
                    
                }
            }
            
        }
        task.resume()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

