//
//  ViewController.swift
//  Guess Answer
//
//  Created by Welborn Machado on 23/08/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    var guess = 1
    
    var correct = 10
    
    @IBOutlet weak var number1: UILabel!
    
    @IBOutlet weak var middlelbl: UILabel!
    
    @IBOutlet weak var number2: UILabel!
    
    @IBOutlet weak var entryfld: UITextField!
    
    @IBOutlet weak var oplbl: UILabel!
    
    @IBOutlet weak var scorelbl: UILabel!
    
    @IBOutlet weak var countlbl: UILabel!
    
    let array1 = ["+","-","*","/"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      
        
    }
    

    @IBAction func gobtn(_ sender: UIButton) {
        var randomno1 = arc4random_uniform(9)
        var randomno2 = arc4random_uniform(9)
        var random = array1[Int(arc4random_uniform(4))]
        var equation = "\(randomno1) \(random) \(randomno2)"
        var exp = NSExpression(format: equation)
        //print(exp.expressionValue(with: nil, context: nil)!)
        
//        number1.text = String(randomno1)
//        number2.text = String(randomno2)
//        middlelbl.text = String(random)
        
        if entryfld.text == String(describing: exp.expressionValue(with: nil, context: nil)! )        {
            print("success")
            oplbl.text = "Right"
            scorelbl.text = String(correct)
            print(correct)
            correct = correct + 10
        }
        else
        {
            print("Not Success")
            oplbl.text = "Wrong"
            countlbl.text = String(guess)
            print(guess)
            guess = guess + 1
            if guess==4 {
                print("next")
                let sb : UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                let vc2 : ViewController2 = sb.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
                self.present(vc2, animated: true, completion: nil)
                
                oplbl.text = " "
            }else{
                print("go on")
            }
        }
        
    }
    
    

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

