//
//  ViewController.swift
//  slidemenudemo
//
//  Created by Welborn Machado on 16/10/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var menu_vc : MenuViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       menu_vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func menubtn(_ sender: UIBarButtonItem) {
        
        if AppDelegate.menu_bool {
            
            show_menu()
            
        }else{
            
            close_menu()
        }
        
        
    }
    
    
    func show_menu()
    {
        UIView.animate(withDuration: 0.3) {() -> Void in
       self.menu_vc.view.frame = CGRect(x: 0, y: 60, width: UIScreen.main.bounds.size.width, height:UIScreen.main.bounds.size.height)
      self.menu_vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
      self.addChildViewController(self.menu_vc)
      self.view.addSubview(self.menu_vc.view)
       AppDelegate.menu_bool = false
            
        }
    }
    
    
    func close_menu()
    {

        
        
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
    self.menu_vc.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 60, width: UIScreen.main.bounds.size.width, height:UIScreen.main.bounds.size.height)
        }) {(finished) in
            
    self.menu_vc.view.removeFromSuperview()
       
    }
    AppDelegate.menu_bool = true
        
    }
    
}



