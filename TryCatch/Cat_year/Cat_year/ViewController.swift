//
//  ViewController.swift
//  Cat_year
//
//  Created by Welborn Machado on 12/08/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var catyrsTF: UITextField!
    
    @IBOutlet weak var displaylbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }

    @IBAction func calculateBtnPressed(_ sender: Any) {
        print("btn pressed "+catyrsTF.text!)
        
       // let catYrs = Int(catyrsTF.text!)!*7
        
        let catYrs : Int = Int(catyrsTF.text!)!*7
        
        
        print(catYrs)
        
        displaylbl.text = "My cat age is"+String(catYrs)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

