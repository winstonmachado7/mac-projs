//
//  ViewController2.swift
//  Cat_year
//
//  Created by Welborn Machado on 12/08/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet weak var signupView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.signupView.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func segmentPressed(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex);
        
        if sender.selectedSegmentIndex==0 {
            self.signupView.isHidden = true
        }else{
            self.signupView.isHidden = false
        }
        
    }

    @IBAction func segmentsPressed(_ sender: UISegmentedControl) {
        
    }
    /*
     
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
