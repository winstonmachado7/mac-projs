//
//  ViewController.swift
//  Holidays
//
//  Created by Welborn Machado on 21/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    @IBOutlet weak var mytableviu: UITableView!

    let myURL = "https://mahindralylf.com/apiv1/getholidays";
    
    
    var monthArray = [String]()
    
    var imageArray = [String]()
    
    var myarr = NSArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getJsonFromUrl()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let nsd : NSDictionary =  myarr.object(at: section) as! NSDictionary
        let arry1 = nsd.value(forKey: "details") as! NSArray
        return arry1.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyTableViewCell
        let nsd : NSDictionary =  myarr.object(at: indexPath.section) as! NSDictionary
        let arry1 = nsd.value(forKey: "details") as! NSArray
        let nsd2  = arry1.object(at: indexPath.row) as! NSDictionary
        cell.mnthlabel.text = nsd2.value(forKey: "title") as! String
        print("sid",nsd)
        
        
        
        return cell
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return myarr.count
    }
    
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        print("winston", myarr.object(at: section) as! NSDictionary)
        let nsd : NSDictionary =  myarr.object(at: section) as! NSDictionary
        let lbl = UILabel()
        lbl.frame = CGRect(x: 0.0, y: 0.0, width: tableView.frame.size.width, height:  tableView.frame.size.width * 0.9)
        lbl.text = nsd.value(forKey: "month") as! String
        lbl.numberOfLines = 0
        //lbl.text = "\(myarr.object(at: section))"
        lbl.backgroundColor = UIColor.green
        lbl.textAlignment = .center
        return lbl
    }
    
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat   {
        return tableView.frame.size.width * 0.9
    }
    
    func getJsonFromUrl(){
        
        let url = NSURL(string: myURL)
        
        
        URLSession.shared.dataTask(with: (url as URL?)!, completionHandler: {(data, response, error) -> Void in
            
            if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary {
                
                
                print(jsonObj!.value(forKey: "holidays")!)
                
                self.myarr = jsonObj!.value(forKey: "holidays") as! NSArray
                print(self.myarr.count)
                self.mytableviu.reloadData()
//                if let heroeArray = jsonObj!.value(forKey: "holidays") as? NSArray {
//                    
//                    for heroe in heroeArray{
//                        
//                        
//                        if let heroeDict = heroe as? NSDictionary {
//                            
//                           
//                            if let name = heroeDict.value(forKey: "month") {
//                                
//                               
//                                self.monthArray.append((name as? String)!)
//                                print(self.monthArray)
//                                
//                            }
//                            
//                            if let name2 = heroeDict.value(forKey: "image") {
//                                
//                                self.imageArray.append((name2 as? String)!)
//                                print(self.imageArray)
//                                
//                            }
//                        }
//                    }
//                }
                
                
                
                            
                            
                            
                            
                OperationQueue.main.addOperation({
                    //calling another function after fetching the json
                    //it will show the names to label
                    //self.showNames()
                })
            }
        }).resume()
    }
        
}


