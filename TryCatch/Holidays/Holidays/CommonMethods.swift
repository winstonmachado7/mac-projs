//
//  CMethods.swift
//  vasaibdirs
//
//  Created by Welborn Machado on 05/07/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import Foundation

func createUserDefault(key:String,data:Any)
{
    let userDefaultObj = UserDefaults.standard
    userDefaultObj.set(data, forKey: key)
    userDefaultObj.synchronize()
}
func getUserdefaultDataForKey(Key:String) -> Any?
{
    let userDefaultObj = UserDefaults.standard
    return userDefaultObj.value(forKey: Key)
}
func convertStringToDictionary(json: String) -> [String: AnyObject]? {
    if let data = json.data(using: String.Encoding.utf8) {
        
        do{
            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: AnyObject]
            return json
        }
        catch
        {
            
        }
    }
    return nil
}
func convertStringToArray(json: String) -> [AnyObject]? {
    if let data = json.data(using: String.Encoding.utf8) {
        
        do{
            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
            return json
        }
        catch
        {
            
        }
    }
    return nil
}
func convertDictionaryToString(json: Any) -> String? {
    
    
    if let theJSONData = try? JSONSerialization.data(
        withJSONObject: json,
        options: []) {
        let theJSONText = String(data: theJSONData,
                                 encoding: .ascii)
        return theJSONText!
    }
    return nil
}
