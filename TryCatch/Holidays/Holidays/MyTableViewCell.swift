//
//  MyTableViewCell.swift
//  Holidays
//
//  Created by Welborn Machado on 23/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    @IBOutlet weak var mnthlabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
