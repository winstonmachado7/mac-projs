//
//  ViewController.swift
//  AudiovideoPlay
//
//  Created by Welborn Machado on 01/10/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ViewController: UIViewController {

    var player = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let htmlLink = ""
        
    }

    @IBAction func localAudioplay(_ sender: Any) {
        let path = Bundle.main.resourcePath!+"/sound.mp3"
        print(path)
        let url = URL(fileURLWithPath: path)
        let playerItem = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: playerItem)
        player.play()
        
    }
    
    @IBAction func playRemoteFile(_ sender: Any) {
        
        let playerItem = AVPlayerItem(url: URL(string: "http://fbdigital.hungama.org/AAD/audio.mp3")!)
        player = AVPlayer(playerItem: playerItem)
        player.play()
        
    }
    @IBAction func localvideoplay(_ sender: Any) {
        let path = Bundle.main.resourcePath!+"/sound.mp4"
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let vc = AVPlayerViewController()
        vc.player = player
        
        present(vc, animated: true, completion: nil)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

