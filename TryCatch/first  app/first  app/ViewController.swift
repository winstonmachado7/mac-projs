//
//  ViewController.swift
//  first  app
//
//  Created by Welborn Machado on 06/08/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("View didload")
        
        /*
         let control = cntrol() // initialization of control
         control.frame = CGRect(x:10,y:20,width:100,height:40) // setting frame of control
         self.view.addsubview(control)//adding on screen
 */
        
        let tf = UITextField()
        tf.frame = CGRect(x: 50, y: 50, width: 100, height: 40)
        self.view.addSubview(tf)
        
       // tf.text = "Trycatch"
        tf.placeholder = "write here"
        
        tf.layer.borderWidth = 1.0
        tf.layer.borderColor = UIColor.black.cgColor
        tf.layer.cornerRadius = 10.0
        tf.isSecureTextEntry = true
        
        let lbl = UILabel()
        lbl.frame = CGRect(x: 50, y: 100, width: 100, height: 40)
        self.view.addSubview(lbl)
        lbl.text = "Type Here"
        lbl.textColor = UIColor.green
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.backgroundColor = UIColor.gray
        lbl.textAlignment = NSTextAlignment.center
        
        let tv = UITextView()
        tv.frame = CGRect(x: 50, y: 150, width: 200, height: 80)
        tv.layer.borderWidth = 1.0
        tv.layer.borderColor = UIColor.black.cgColor
        self.view.addSubview(tv)
        
        let btn = UIButton()
        btn.frame = CGRect(x: 50, y: 240, width: 100, height: 30)
        btn.layer.borderWidth = 1.0
        btn.layer.borderColor = UIColor.black.cgColor
        self.view.addSubview(btn)
        
        btn.setTitleColor(UIColor.black, for: .normal)
        btn.setTitle("Click Here", for: .normal)
        btn.alpha = 0.5
        btn.backgroundColor = UIColor.red
        
        let imgV  = UIImageView()
        imgV.frame = CGRect(x: 50, y: 290, width: 60, height: 60)
        self.view.addSubview(imgV)
        imgV.image = UIImage.init(named: "HighPitch.png")
        
        let sldr = UISlider()
        sldr.frame = CGRect(x: 50, y: 340, width: 200, height: 50)
        self.view.addSubview(sldr)
        sldr.minimumValue = 0.0
        sldr.maximumValue = 15.0
        sldr.value = 3.0
        
        let swtch = UISwitch()
        swtch.frame = CGRect(x: 50, y: 390, width: 100, height: 70)
        self.view.addSubview(swtch)
        swtch.isOn = true
        
        let seg = UISegmentedControl()
        seg.frame = CGRect(x: 50, y: 440, width: 150, height: 40)
        
        self.view.addSubview(seg)
        
        seg.insertSegment(withTitle: "HollyWood", at: 0, animated: true)
        seg.insertSegment(withTitle: "BollyWood", at: 1, animated: true)
        seg.selectedSegmentIndex = 0

        let actInd = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 200, y: 390, width: 40, height: 40)
        actInd.color = UIColor.red
        self.view.addSubview(actInd)
        actInd.startAnimating()
        
        let pkr = UIPickerView()
        pkr.frame = CGRect(x: 50, y: 490, width: 200, height: 50)
        pkr.backgroundColor = UIColor.blue
        self.view.addSubview(pkr)
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

