//
//  ViewController2.swift
//  login signup
//
//  Created by Welborn Machado on 13/08/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    var valuePassed:String!
    
    @IBOutlet weak var welcomeLbl: UILabel!
    
    @IBOutlet weak var webview: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.welcomeLbl.text = valuePassed
        
        // Do any additional setup after loading the view.
        webview.loadRequest(URLRequest.init(url: URL.init(string: valuePassed)!))
    }
    @IBAction func backPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
