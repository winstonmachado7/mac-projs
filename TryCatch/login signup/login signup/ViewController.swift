//
//  ViewController.swift
//  login signup
//
//  Created by Welborn Machado on 07/08/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var sw: UISwitch!
    @IBOutlet weak var emTF: UITextField!
    @IBOutlet weak var pwdTF: UITextField!
    @IBOutlet weak var aboutTV: UITextView!
    
    let name = UITextField()
    let emailid = UITextField()
    let pass1 = UITextField()
    let confirm = UITextField()
    let signup = UIButton()
    
    
    
    let user1 = UITextField()
    let pass = UITextField()
    let login = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        aboutTV.layer.borderWidth = 1.0
        aboutTV.layer.borderColor = UIColor.lightGray.cgColor
        
        // Do any additional setup after loading the view, typically from a nib.
        
//        let seg = UISegmentedControl()
//        seg.frame = CGRect(x: 35, y: 50, width: 250, height: 40)
//        seg.insertSegment(withTitle: "Sign up", at: 0, animated: true)
//        seg.insertSegment(withTitle: "Login", at: 1, animated: true)
//        seg.layer.cornerRadius = 2.0
//        seg.selectedSegmentIndex = 0
//        seg.addTarget(self, action: #selector(changeseg), for: .valueChanged)
//        self.view.addSubview(seg)
//        
//        // Sign Up Page
//        
//        name.frame = CGRect(x: 35, y: 150, width: 250, height: 40)
//        name.placeholder = "Name"
//        name.textAlignment = NSTextAlignment.center
//        name.layer.cornerRadius = 3.0
//        name.layer.borderColor = UIColor.gray.cgColor
//        name.layer.borderWidth = 1.0
//        self.view.addSubview(name)
//        
//        
//        
//        emailid.frame = CGRect(x: 35, y: 210, width: 250, height: 40)
//        emailid.placeholder = "Email Id"
//        emailid.textAlignment = NSTextAlignment.center
//        emailid.layer.cornerRadius = 3.0
//        emailid.layer.borderColor = UIColor.gray.cgColor
//        emailid.layer.borderWidth = 1.0
//        self.view.addSubview(emailid)
//        
//        
//        pass1.frame = CGRect(x: 35, y: 270, width: 250, height: 40)
//        pass1.placeholder = "Password"
//        pass1.isSecureTextEntry = true
//        pass1.textAlignment = NSTextAlignment.center
//        pass1.layer.cornerRadius = 3.0
//        pass1.layer.borderColor = UIColor.gray.cgColor
//        pass1.layer.borderWidth = 1.0
//        self.view.addSubview(pass1)
//        
//        
//        confirm.frame = CGRect(x: 35, y: 330, width: 250, height: 40)
//        confirm.placeholder = "Confirm Password"
//        confirm.isSecureTextEntry = true
//        confirm.textAlignment = NSTextAlignment.center
//        confirm.layer.cornerRadius = 3.0
//        confirm.layer.borderColor = UIColor.gray.cgColor
//        confirm.layer.borderWidth = 1.0
//        self.view.addSubview(confirm)
//        
//        
//        signup.setTitle("Sign Up", for: .normal)
//        signup.setTitleColor(UIColor.white, for: .normal)
//        signup.setTitleColor(UIColor.green, for: .highlighted)
//        signup.backgroundColor = UIColor.blue
//        signup.layer.cornerRadius = 5.0
//        signup.frame = CGRect(x: 100, y: 420, width: 120, height: 40)
//        signup.alpha = 0.5
//        
//        self.view.addSubview(signup)
        
    }
    
    func changeseg(sender: UISegmentedControl) {
        
        login.alpha = 0.0
        user1.alpha = 0.0
        pass.alpha = 0.0
        confirm.alpha = 0.0
        signup.alpha = 0.0
        emailid.alpha = 0.0
        name.alpha = 0.0
        pass1.alpha = 0.0
        
        if sender.selectedSegmentIndex == 0 {
            
            UIView.animate(withDuration: 1.0, animations: {
                
                self.name.alpha = 1.0
                self.emailid.alpha = 1.0
                self.pass1.alpha = 1.0
                self.confirm.alpha = 1.0
                self.signup.alpha = 0.5
                
            }, completion: nil)
            
            name.isHidden = false
            emailid.isHidden = false
            pass1.isHidden = false
            confirm.isHidden = false
            signup.isHidden = false
            user1.isHidden = true
            pass.isHidden = true
            login.isHidden = true
            
         }else{
            
            UIView.animate(withDuration: 1.0, animations: {
                
                self.user1.alpha = 1.0
                self.pass.alpha = 1.0
                self.login.alpha = 1.0
                
                
            }, completion: nil)
            
            name.isHidden = true
            emailid.isHidden = true
            pass1.isHidden = true
            confirm.isHidden = true
            signup.isHidden = true
            user1.isHidden = false
            pass.isHidden = false
            login.isHidden = false
            
            user1.frame = CGRect(x: 35, y: 210, width: 250, height: 40)
            user1.placeholder = "Email Id"
            user1.textAlignment = NSTextAlignment.center
            user1.layer.cornerRadius = 3.0
            user1.layer.borderColor = UIColor.gray.cgColor
            user1.layer.borderWidth = 1.0
            self.view.addSubview(user1)
            
            
            pass.frame = CGRect(x: 35, y: 270, width: 250, height: 40)
            pass.placeholder = "Password"
            pass.isSecureTextEntry = true
            pass.textAlignment = NSTextAlignment.center
            pass.layer.cornerRadius = 3.0
            pass.layer.borderColor = UIColor.gray.cgColor
            pass.layer.borderWidth = 1.0
            self.view.addSubview(pass)
            
            
            login.setTitle("Log In", for: .normal)
            login.setTitleColor(UIColor.white, for: .normal)
            login.setTitleColor(UIColor.green, for: .highlighted)
            login.backgroundColor = UIColor.blue
            login.layer.cornerRadius = 5.0
            login.frame = CGRect(x: 100, y: 360, width: 120, height: 40)
            login.alpha = 0.5
            self.view.addSubview(login)
            
            }
        
       // let alert = UIAlertView()
    }
    
    
    @IBAction func signUppressed(_ sender: UIButton) {
        cleartTF(sender: emTF)
        cleartTF(sender: pwdTF)

        if emTF.text!.isEmpty{
            highlightTF(sender: emTF)
        }else if(pwdTF.text!.characters.count<7){
            highlightTF(sender: pwdTF)
        }else if aboutTV.text!.isEmpty{
            highlightTV(sender: aboutTV)
        }else{
//            let alert = UIAlertController(title: "Success", message: "SignUp Done!", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//            self.present(alert, animated: false, completion: nil)
//            
           // let alert = UIAlertView(title: "Success", message: "SignUp Done!", delegate: nil, cancelButtonTitle: "OK")
           // alert.show()
            
           // let vc:UIViewController = UIViewController.init(nibName: ], bundle: <#T##Bundle?#>)
            
            /********Creted with class xib ******/
            //let vc : UIViewController = UIViewController.init(nibName: "ViewController3", bundle: nil)
            //self.present(vc, animated: true, completion: nil)
            
            let sb : UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = sb.instantiateViewController(withIdentifier: "ViewController2")
            self.present(vc, animated: true, completion: nil)
            
            
        }
        
    }
    
    func cleartTF(sender:UITextField) -> Void {
        sender.layer.borderWidth = 1.0
        sender.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func highlightTF(sender:UITextField) -> Void {
        sender.layer.borderWidth = 1.0
        sender.layer.borderColor = UIColor.red.cgColor
    }
    func highlightTV(sender:UITextView) -> Void {
        sender.layer.borderWidth = 1.0
        sender.layer.borderColor = UIColor.red.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func button1Press(_ sender: UIButton) {
        let sb : UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc : ViewController2 = sb.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
        vc.valuePassed = "https://www.google.co.in"
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func button2press(_ sender: Any) {
        let sb : UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc : ViewController2 = sb.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
        vc.valuePassed = "https://www.yahoo.com/"
        self.present(vc, animated: true, completion: nil)

    }
}

        
        
        
        

    


