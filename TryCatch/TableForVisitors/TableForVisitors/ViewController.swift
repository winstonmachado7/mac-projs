//
//  ViewController.swift
//  TableForVisitors
//
//  Created by Welborn Machado on 03/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.myTableView.delegate = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 5;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = "Hello"
        return cell;
        
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3;
    }
    
     public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 28))
        lbl.text  = "Header Start"
        lbl.backgroundColor = UIColor.init(red: 14.0/255.0, green: 136.0/255.0, blue: 86.0/255.0, alpha: 1.0)
        return lbl
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 28.0
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?{
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 28))
        lbl.text  = "Header END"
        lbl.backgroundColor = UIColor.init(red: 114.0/255.0, green: 136.0/255.0, blue: 186.0/255.0, alpha: 1.0)
        return lbl
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 28.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

