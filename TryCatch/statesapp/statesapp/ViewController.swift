//
//  ViewController.swift
//  statesapp
//
//  Created by Welborn Machado on 30/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tabledata: UITableView!
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    let myurl = "http://stag-kalyan.com/api/city_listing.php"
    
    var myarr = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       // updateDetails()
        
        collapi();
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5 //myarr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell2", for: indexPath) as! TableViewCell2
        
        
        return cell
    }
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func collapi() -> Void {
        
        actInd.startAnimating()
        self.view.isUserInteractionEnabled = false
        
        let urlrequest = NSMutableURLRequest(url: URL(string: "http://stag-kalyan.com/api/city_listing.php")!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 30)
        let postStr = "request=city_listing&device_type=ios"
         urlrequest.httpBody = postStr.data(using: .utf8)
        urlrequest.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: urlrequest as URLRequest) { data,response,error in
            
            if let datares = data{
                do{
                let dict = try JSONSerialization.jsonObject(with: datares, options: []) as! NSDictionary
                    print("Dict is ",dict)
                }catch let error{
                    print(error.localizedDescription)
                }
            }
            
            DispatchQueue.main.async {
                self.actInd.stopAnimating()
                self.view.isUserInteractionEnabled = true
            }
        }
        task.resume()
}

    
    
    
    
    func updateDetails()
    {
        
        let postString = "request=city_listing&device_type=ios"
        
        print(postString)
        
        
        
        let url = NSURL(string: "http://stag-kalyan.com/api/city_listing.php")
        
        let request = NSMutableURLRequest(url: url! as URL)
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        request.httpMethod = "POST"
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data,response,error in
            guard error == nil && data != nil else
            {
                
                print("Error:\(error)")
                return
            }
            
            let httpStatus = response as? HTTPURLResponse
            
            if httpStatus!.statusCode == 200
            {
                if let dataRes = data
                {
                    //let responseString = String(data: data!, encoding: .utf8)
                    //print(responseString)
                    do{
                    let jsonResp = try JSONSerialization.jsonObject(with: dataRes, options: []) as! NSDictionary
                        print(jsonResp)
                    }catch {
                        print("err")
                    }
                    DispatchQueue.main.async {
                        //self.tabledata.reloadData()
                    }
                }
                else
                {
                    print("No data got from url!")
                    
                }
            }
            else
            {
                
                print("error httpstatus code")
            }
        }
        task.resume()
        
    }
    


}

