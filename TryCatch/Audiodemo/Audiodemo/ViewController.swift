//
//  ViewController.swift
//  Audiodemo
//
//  Created by Welborn Machado on 24/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    var player = AVAudioPlayer()
    
    @IBOutlet weak var playlocalPress: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func playLocalPress(_ sender: Any) {
        let path = Bundle.main.path(forResource: "local", ofType: "mp3")
        do{
        player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path!))
            player.play()
        }catch{
            print("Exception")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

