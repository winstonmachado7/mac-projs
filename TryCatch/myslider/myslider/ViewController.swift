//
//  ViewController.swift
//  myslider
//
//  Created by Welborn Machado on 06/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var result1: UILabel!
    @IBOutlet weak var lab1: UILabel!
    @IBOutlet weak var slideme: UISlider!
    @IBOutlet weak var result2: UILabel!
    @IBOutlet weak var result3: UILabel!
    @IBOutlet weak var result4: UILabel!
    @IBOutlet weak var result5: UILabel!
    @IBOutlet weak var result6: UILabel!
    @IBOutlet weak var result7: UILabel!
    @IBOutlet weak var result8: UILabel!
    @IBOutlet weak var result9: UILabel!
    @IBOutlet weak var result10: UILabel!
    @IBOutlet weak var lab2: UILabel!
    @IBOutlet weak var lab3: UILabel!
    @IBOutlet weak var lab4: UILabel!
    @IBOutlet weak var lab5: UILabel!
    @IBOutlet weak var lab6: UILabel!
    @IBOutlet weak var lab7: UILabel!
    @IBOutlet weak var lab8: UILabel!
    @IBOutlet weak var lab9: UILabel!
    @IBOutlet weak var lab10: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    @IBAction func slidermov(_ sender: Any) {
        
        let value1 = slideme.value
        
        lab1.text = String(Int(value1))
        result1.text = String(Int(value1) * 1)
        
        lab2.text = String(Int(value1))
        result2.text = String(Int(value1) * 2)
        
        lab3.text = String(Int(value1))
        result3.text = String(Int(value1) * 3)
        
        lab4.text = String(Int(value1))
        result4.text = String(Int(value1) * 4)
        
        lab5.text = String(Int(value1))
        result5.text = String(Int(value1) * 5)
        
        lab6.text = String(Int(value1))
        result6.text = String(Int(value1) * 6)
        
        lab7.text = String(Int(value1))
        result7.text = String(Int(value1) * 7)
        
        lab8.text = String(Int(value1))
        result8.text = String(Int(value1) * 8)
        
        lab9.text = String(Int(value1))
        result9.text = String(Int(value1) * 9)
        
        lab10.text = String(Int(value1))
        result10.text = String(Int(value1) * 10)
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

