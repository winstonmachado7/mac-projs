//
//  ViewController.swift
//  next_api_pro
//
//  Created by Apple on 30/09/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mytabledata: UITableView!
    
    let url2 = "http://www.voipcom-hd.ch/notification/document.json"
    
    var myarr = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mdata2()
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return myarr.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! myTableViewCell
        let nsd = myarr.object(at:  indexPath.row) as! NSDictionary
        cell.validlbl.text = nsd.value(forKey: "validity") as! String?
        cell.txfda.text = nsd.value(forKey: "description") as! String?
        cell.titlelbl.text = nsd.value(forKey: "title") as! String?
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
      
      let nsd = myarr.object(at:  indexPath.row) as! NSDictionary
       
    let url = nsd.value(forKey: "link")
        
    UIApplication.shared.open(URL(string: url as! String)!, options:["":""], completionHandler: nil)
        
    }
    
    
    
    func mdata2() {
        let url = URL(string: url2)
        URLSession.shared.dataTask(with:url!) { (data, response, error) in
            if error != nil {
                print(error)
            } else {
                do {
                    
                    //let parsedData = try JSONSerialization.jsonObject(with: data!) as! [String:Any]
                    let dict = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    self.myarr = dict.value(forKey: "notifications") as! NSArray
                    print(self.myarr)
                    DispatchQueue.main.async {
                        self.mytabledata.reloadData()
                    }
                    
                    //print(self.myarr)
                } catch let error as NSError {
                    print(error)
                }
            }
            
            }.resume()
        
    }

}

