//
//  ViewController.swift
//  table&section
//
//  Created by Welborn Machado on 09/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var ourtv: UITableView!
    
    var hidden:[Bool] = [true, true]
    
    var array1 = ["hiiiiii"]
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = array1[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel()
        label.textAlignment = .left
        label.text = "hi"
        label.tag = section
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.tapFunction))
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(tap)
        
        return label
    }
    
    func tapFunction(sender:UITapGestureRecognizer) {
        let section = sender.view!.tag
        let indexPaths = (0..<26).map { i in return IndexPath(item: i, section: section)  }
        
        hidden[section] = !hidden[section]
        
        ourtv?.beginUpdates()
        if hidden[section] {
            ourtv?.deleteRows(at: indexPaths, with: .top)
        } else {
            ourtv?.insertRows(at: indexPaths, with: .top)
        }
        ourtv?.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hidden[section] {
            return 0
        } else {
            return 26
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

