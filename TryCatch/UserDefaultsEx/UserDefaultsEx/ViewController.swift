//
//  ViewController.swift
//  UserDefaultsEx
//
//  Created by Welborn Machado on 16/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var unTF: UITextField!
    @IBOutlet weak var unLbl: UILabel!
    @IBOutlet weak var imgV: UIImageView!
    var arr = NSMutableArray();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func saveBtnPress(_ sender: Any) {
 //       UserDefaults.init().set(unTF.text, forKey: "username")
        
      /*  let picker = UIImagePickerController.init()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
        */
        
        arr.add(unTF.text!)
//        UserDefaults.init().set(arr, forKey: "username")
//        UserDefaults.init().synchronize()
        
        UserDefaults.standard.removeObject(forKey: "username1")
        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.set(arr, forKey: "username1")
        //print(UserDefaults.init().value(forKey: "username") as! NSMutableArray)
        
    
        print(UserDefaults.standard.object(forKey: "username1"))
        
    }
    
    
     public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
     {
        let img = info[UIImagePickerControllerOriginalImage] as! UIImage
        imgV.image = img
        dismiss(animated: true, completion: nil)
    }
    
   public func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
   {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func getBtnPress(_ sender: Any) {
        if let savedUn = UserDefaults.init().value(forKey: "username") {
            unLbl.text = savedUn as? String
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

