//
//  ViewController.swift
//  NavigationBarDemo
//
//  Created by Welborn Machado on 10/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITabBarDelegate {

    @IBOutlet weak var tabBar: UITabBar!
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tabBar.delegate = self
    }

    func processTimer() -> Void {
        print("processTimer")

    }
    
    @IBAction func playPressed(_ sender: Any) {
        print("play press")
        
        
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(processTimer), userInfo: nil, repeats: true)
    }
    
    @IBAction func cameraPress(_ sender: Any) {
        print("camera press")
        timer.invalidate()
    }
    
    
    public func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem){
        print("selected")
        print(tabBar.selectedItem!.tag)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

