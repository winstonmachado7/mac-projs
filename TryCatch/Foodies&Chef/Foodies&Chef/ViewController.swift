//
//  ViewController.swift
//  Foodies&Chef
//
//  Created by Welborn Machado on 19/08/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func Aboutus(_ sender: Any) {
        let sb : UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc : aboutusViewController = sb.instantiateViewController(withIdentifier: "aboutusViewController") as! aboutusViewController
        self.present(vc, animated: true, completion: nil)
    }

    @IBAction func Connect(_ sender: Any) {
        let sb : UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc1 : connectViewController = sb.instantiateViewController(withIdentifier: "connectViewController") as! connectViewController
        self.present(vc1, animated: true, completion: nil)
    }
    
    
    @IBAction func Order(_ sender: Any) {
        let sb : UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc2 : orderViewController = sb.instantiateViewController(withIdentifier: "orderViewController") as! orderViewController
        vc2.valuepassed = "http://www.codigocreative.info/foodie/preview.php"
        self.present(vc2, animated: true, completion: nil)
    }
    
    @IBAction func Findus(_ sender: Any) {
        let sb : UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc3 : findusViewController = sb.instantiateViewController(withIdentifier: "findusViewController") as! findusViewController
        self.present(vc3, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

