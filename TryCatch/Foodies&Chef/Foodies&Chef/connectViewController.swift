//
//  connectViewController.swift
//  Foodies&Chef
//
//  Created by Welborn Machado on 19/08/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class connectViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    @IBAction func facebook(_ sender: Any) {
        let sb : UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc2 : orderViewController = sb.instantiateViewController(withIdentifier: "orderViewController") as! orderViewController
        vc2.valuepassed = "https://www.facebook.com/thefoodieandthechef"
        self.present(vc2, animated: true, completion: nil)
    }
    

    @IBAction func twitter(_ sender: Any) {
        let sb : UIStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc2 : orderViewController = sb.instantiateViewController(withIdentifier: "orderViewController") as! orderViewController
        vc2.valuepassed = "https://twitter.com/foodiechefs"
        self.present(vc2, animated: true, completion: nil)
        
    }
    
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
