//
//  orderViewController.swift
//  Foodies&Chef
//
//  Created by Welborn Machado on 19/08/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class orderViewController: UIViewController {
    
    @IBOutlet weak var activityind: UIActivityIndicatorView!
    
    var valuepassed : String!

    @IBOutlet weak var webview: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webview.loadRequest(URLRequest(url: URL(string: valuepassed)!))
        // Do any additional setup after loading the view.
    }

    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @available(iOS 2.0, *)
    public func webViewDidStartLoad(_ webView: UIWebView){
        print("webViewDidStartLoad");
    }
    
    @available(iOS 2.0, *)
    public func webViewDidFinishLoad(_ webView: UIWebView){
        print("webViewDidFinishLoad");
        activityind.stopAnimating()
        
    }
    
    @available(iOS 2.0, *)
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        print("didFailLoadWithError",error.localizedDescription);
        activityind.stopAnimating()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
