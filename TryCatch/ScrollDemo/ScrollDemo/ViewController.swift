//
//  ViewController.swift
//  ScrollDemo
//
//  Created by Welborn Machado on 02/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var fnTF: UITextField!
    @IBOutlet weak var lnTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        print("keyboard return")
        if textField==fnTF {
            lnTF.becomeFirstResponder()
        }else{
            lnTF.resignFirstResponder()
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

