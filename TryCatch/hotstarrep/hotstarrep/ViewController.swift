//
//  ViewController.swift
//  hotstarrep
//
//  Created by Welborn Machado on 10/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var hstable: UITableView!
    
    let titlearr = [" ","TV Shows For You","Movies For You","Popular Premium TV Shows","Popular Premium Movies","Live Sports For You"]
    
    let imgarr0 = ["main0.jpg","main1.jpg","main2.jpg","main4.jpg","main3.jpg"]
    
    let labarr0 = ["Narcos Season 3","Live:Ind vs Sri","Taarak Mehta Ka Oolta Chashma","Wonder Woman","AD: Hotstar Go Solo"]

    let imgarr1 = ["show1.jpg","show2.jpg","show3.jpg","show4.jpg","show5.jpeg"]
    
    let labarr1 = ["Yeh Rishta Kya Kehlata Hai","Iss Pyaar Ko Kya Naaam Du","Kya Hal Mr.Panchaal","Rishtoh Ka Chakrayug","Dance 3+"]
    
    let imgarr2 = ["movie1.jpeg","movie2.jpg","movie3.jpg","movie4.jpg","movie5.jpg"]
    
    let labarr2 = ["M.S.Dhoni:The Untold Story","Jolly LLB 2","Kaabil","Housefull 3","Singham Returns"]
    
    let imgarr3 = ["premi1.jpeg","premi2.jpg","premi3.jpg","premi4.jpg","premi5.jpg"]
    
    let labarr3 = ["Game Of Thrones:S7","Modern Family","Narcos","Ballers","WestWorld"]
    
    let imgarr4 = ["pmov1.jpeg","pmov2.jpg","pmov3.jpeg","pmov4.jpeg","pmov5.jpg"]
    
    let labarr4 = ["DeadPool","Kingsman: The Secret Service","Hitman: Agent 47","X-Men Apocalyse","Kung Fu Panda 3"]
    
    let imgarr5 = ["main1.jpg","sports2.jpg","sports3.jpg","sports4.jpeg","sports5.jpg"]
    
    let labarr5 = ["India vs SriLanka Tests","Pro Kabaddi:TT vs Delhi Tigers","Rafael Nadal:Tennis star","Chelsea vs Arsenal","Sainal Nehwal strikes Back"]
    
    let arr = ["1","2","3","4","5"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = arr.count
       
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "Menu"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        let item1 = UIBarButtonItem(customView: btn1)
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "Search"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
        
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HSTableViewCell") as! HSTableViewCell
        cell.HSCV.delegate = self
        cell.HSCV.dataSource = self
        cell.HSCV.tag = indexPath.section
        
        return cell
        
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 150.0
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView.tag == 0{
            return imgarr0.count
        } else if collectionView.tag == 1{
            return imgarr1.count
        } else if collectionView.tag == 2{
            return imgarr2.count
        } else if collectionView.tag == 3{
            return imgarr3.count
        }else if collectionView.tag == 4{
            return imgarr4.count
        }else if collectionView.tag == 5{
            return imgarr5.count
        }   else
        {
            return 0
        }
            }
    
    public func numberOfSections(in tableView: UITableView) -> Int{
       return 6
        
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HSCollectionViewCell", for: indexPath) as! HSCollectionViewCell
        if collectionView.tag == 0 {
            cell.imgviu.image = UIImage.init(named: imgarr0[indexPath.row])
            cell.deslbl.text = labarr0[indexPath.row]
            if indexPath.row == 0{
            cell.premiumlbl.isHidden = false
            }
            else if indexPath.row == 3
            {
            cell.premiumlbl.isHidden = false
            }
            else
            {
             cell.premiumlbl.isHidden = true
            }
            
            }
    
          else if collectionView.tag == 1{
            cell.imgviu.image = UIImage.init(named: imgarr1[indexPath.row])
            cell.deslbl.text = labarr1[indexPath.row]
            cell.premiumlbl.isHidden = true
        } else if collectionView.tag == 2 {
            cell.imgviu.image = UIImage.init(named: imgarr2[indexPath.row])
            cell.deslbl.text = labarr2[indexPath.row]
            cell.premiumlbl.isHidden = true
        } else if collectionView.tag == 3{
            cell.imgviu.image = UIImage.init(named: imgarr3[indexPath.row])
            cell.deslbl.text = labarr3[indexPath.row]
            cell.premiumlbl.isHidden = false
        }else if collectionView.tag == 4{
            cell.imgviu.image = UIImage.init(named: imgarr4[indexPath.row])
            cell.deslbl.text = labarr4[indexPath.row]
            cell.premiumlbl.isHidden = false
        }else if collectionView.tag == 5{
            cell.imgviu.image = UIImage.init(named: imgarr5[indexPath.row])
            cell.deslbl.text = labarr5[indexPath.row]
            cell.premiumlbl.isHidden = true
        }
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView.tag == 0 {
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        }else if collectionView.tag == 5 {
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        }
        return CGSize(width: collectionView.frame.size.height, height: collectionView.frame.size.height)
        }
    
    
    
   public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
    let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 12))
    lbl.textAlignment = NSTextAlignment.left
    lbl.text = titlearr[section]
    return lbl
    
  }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if section == 0{
            return 0.0
        }
        return 30.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let sb = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let view = sb.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
        if collectionView.tag == 0{
            view.imagePath = imgarr0[indexPath.row]
            view.labpath = labarr0[indexPath.row]
            if indexPath.row == 0{
                view.visible = false
            }
            else if indexPath.row == 3
            {
                view.visible = false
            }
            else
            {
                view.visible = true
            }
        }
        if collectionView.tag == 1{
            view.imagePath = imgarr1[indexPath.row]
            view.labpath = labarr1[indexPath.row]
            view.visible = true
        }
        if collectionView.tag == 2{
            view.imagePath = imgarr2[indexPath.row]
            view.labpath = labarr2[indexPath.row]
            view.visible = true
        }
        if collectionView.tag == 3{
            view.imagePath = imgarr3[indexPath.row]
            view.labpath = labarr3[indexPath.row]
            view.visible = false
        }
        if collectionView.tag == 4{
            view.imagePath = imgarr4[indexPath.row]
            view.labpath = labarr4[indexPath.row]
            view.visible = false
        }
        if collectionView.tag == 5{
            view.imagePath = imgarr5[indexPath.row]
            view.labpath = labarr5[indexPath.row]
            view.visible = true
        }
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

