//
//  ViewController2.swift
//  hotstarrep
//
//  Created by Welborn Machado on 14/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {
    
    @IBOutlet weak var lbl2: UILabel!
    
    @IBOutlet weak var desImg: UIImageView!
    
    @IBOutlet weak var premen: UILabel!
    
    @IBOutlet weak var download: UIButton!
    
    @IBOutlet weak var share: UIButton!
    
    var imagePath : String = " "
    
    var labpath : String = " "
    
    var visible = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl2.text = labpath
        
        desImg.image = UIImage.init(named: imagePath)
        
        download.layer.cornerRadius = 3.0
        
        share.layer.cornerRadius = 3.0
        
        premen.isHidden = visible
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
