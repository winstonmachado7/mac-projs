//
//  RecordSoundsViewController.swift
//  practice
//
//  Created by Welborn Machado on 08/06/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit
import AVFoundation

class RecordSoundsViewController: UIViewController, AVAudioRecorderDelegate {
    
    var audioRecorder: AVAudioRecorder!

    @IBOutlet weak var recordinglabel: UILabel!
    @IBOutlet weak var recordbutton: UIButton!
    @IBOutlet weak var stoprecordingbutton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stoprecordingbutton.isEnabled = false
            }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    @IBAction func recordaudio(_ sender: AnyObject) {
        recordinglabel.text = "Recording in Progress"
        recordbutton.isEnabled = false
        stoprecordingbutton.isEnabled = true
        
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let recordingName = "recordedVoice.wav"
        let pathArray = [dirPath, recordingName]
        let filePath = URL(string: pathArray.joined(separator: "/"))
    
        
        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
        try! audioRecorder = AVAudioRecorder(url: filePath!, settings: [:])
        audioRecorder.delegate = self
        audioRecorder.isMeteringEnabled = true
        audioRecorder.prepareToRecord()
        audioRecorder.record()
    }

    @IBAction func stoprecording(_ sender: AnyObject) {
        recordbutton.isEnabled = true
        stoprecordingbutton.isEnabled = false
        recordinglabel.text = "Tap to Record"
        audioRecorder.stop()
        let audioSession = AVAudioSession.sharedInstance()
        try! audioSession.setActive(false)
            }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if flag {
            performSegue(withIdentifier: "stoprecording" , sender: audioRecorder.url)
        }
        else {
            print("Recording was not successful")
        }
            }


override func prepare(for segue: UIStoryboardSegue, sender: Any?){
    if segue.identifier == "stoprecording" {
    let playSoundsVC = segue.destination as! PlaySoundsViewController
    let recordedAudioURL = sender as! URL
    playSoundsVC.recordedAudioURL = recordedAudioURL
        
    }
    
    }
    
}














