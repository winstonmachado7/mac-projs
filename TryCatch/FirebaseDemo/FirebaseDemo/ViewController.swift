//
//  ViewController.swift
//  FirebaseDemo
//
//  Created by Welborn Machado on 17/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        PhoneAuthProvider.provider().verifyPhoneNumber("9892426230") { (verificationID, error) in
            if let error = error {
                print(error)
                return
            }
            // Sign in using the verificationID and the code sent to the user
            // ...
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

