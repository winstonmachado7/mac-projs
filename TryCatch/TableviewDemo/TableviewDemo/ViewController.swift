//
//  ViewController.swift
//  TableviewDemo
//
//  Created by Welborn Machado on 02/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var myTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //self.myTableView.estimatedRowHeight = 130.0
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 5;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "cell")
        cell.textLabel?.text = "test"
        cell.imageView?.image = UIImage.init(named: "test.png")
        cell.detailTextLabel?.text = "description ehwrewjkfr rew,mjfretkejrklgt r,.lewkjtgler t.,rekgtyl;rel tyl./r;hkrtkhul;rtkhyl;rtkhyl;krthl;ktrl;hykrtlhkl;tykhl56654455454545454254254254252525254254254"
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.sizeToFit()
        return cell
        
    }
    
     public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        print("row height")
        return 120.0//UITableViewAutomaticDimension
    }
    
//    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
//        return 90.0
//    }
    
     public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
            print(indexPath.row)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

