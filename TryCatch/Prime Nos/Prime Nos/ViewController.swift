//
//  ViewController.swift
//  Prime Nos
//
//  Created by Welborn Machado on 02/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textfld: UITextField!
    
    @IBOutlet weak var lbl: UILabel!
    
    @IBOutlet weak var subtn: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        subtn.layer.cornerRadius = 4.0
    }
    
    
    @IBAction func Gobtn(_ sender: Any) {
        if let userentry = textfld.text{
            
            let userEnteredInteger = Int(userentry)
            
            
            if let number = userEnteredInteger{
                
                var isPrime = true
                
                if number == 1{
                    isPrime = false
                }
                var i = 2
                
                while i < number{
                    
                    if number % i == 0{
                        isPrime = false
                    }
                    
                    i += 1
                }
               
                if isPrime{
                    lbl.text = "\(number) is a prime number!"
                }
                    
                else{
                    lbl.text = "\(number) is not a prime number"
                }
            }
            else{
                lbl.text = "Please enter a number."
            }
        }
        self.view.endEditing(true)
        }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        lbl.text = " "
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

