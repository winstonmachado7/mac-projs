//
//  ViewController.swift
//  CoredataDemo
//
//  Created by Welborn Machado on 08/10/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var taskTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func savePressed(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let task = TaskItem(context: context)
        task.taskname = taskTF.text ?? "blank"
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        taskTF.text = ""
    }
    
    @IBAction func getPressed(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do{
            let items = try  context.fetch(TaskItem.fetchRequest())
            print(items)
            for i in 0..<items.count{
                let item = items[i] as! TaskItem
                print(" :",item.taskname!)
            }
           
            
        }catch{
            print("err")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

