//
//  ViewController.swift
//  passdatatry
//
//  Created by Welborn Machado on 20/06/18.
//  Copyright © 2018 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var valuelbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        valuelbl.isHidden = true
    }

   
    @IBAction func pushvc(_ sender: UIButton) {
        let NewVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController2") as? ViewController2
        NewVC?.delegate = self
        self.navigationController?.pushViewController(NewVC!, animated: true)
    }
    
  
}

extension ViewController : myproto {
    func valsend(info: String,info2:String) {
        print(info)
        if info2 == "unhidden"
        {
        valuelbl.isHidden = false
        valuelbl.text = info
        }
    }
    
    
}
