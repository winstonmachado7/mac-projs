//
//  ViewController.swift
//  customtbvcell
//
//  Created by Apple on 03/09/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    let imgarr = ["pro img.png","profem.jpg","colorpro.jpg"]
    let labarr = ["Person 1","Person2","Person 3"]
    let txview = ["This is a post ghjhkjjhgcgbjkmk","Post Something Here hderfujohgf","This is everyone's Post uhijhrtfyu"]
    var array1 = [["desc":"Hi i am person 1","image":"pro img.png"],["desc":"Hi i am person 2","image":"profem.jpg"],["desc":"Hi i am person 3","image":"colorpro.jpg"]]
    
    @IBOutlet weak var tablevw: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 3
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell : FBTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell0") as! FBTableViewCell
        
        cell.lbl.text = labarr[indexPath.row]
        cell.profileimg.image = UIImage.init(named:imgarr[indexPath.row])
        cell.statuslbl.text = txview[indexPath.row]
        cell.profileimg.layer.cornerRadius = 35.0
        cell.profileimg.clipsToBounds = true
        cell.like.layer.cornerRadius = 2.0
        cell.like.clipsToBounds = true
        cell.comment.layer.cornerRadius = 2.0
        cell.comment.clipsToBounds = true
        cell.share.layer.cornerRadius = 2.0
        cell.share.clipsToBounds = true
        cell.post.layer.cornerRadius = 2.0
        cell.post.clipsToBounds = true
        cell.typhere.isHidden = true
        cell.post.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "Send", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Send"
        {
            let viewC = segue.destination as! detailViewController
            let dictionaryWorld = array1[sender as! Int] as! NSMutableDictionary
            viewC.valuepassed = (dictionaryWorld.value(forKey: "desc") as? String)!
            viewC.valuepassed2 = (dictionaryWorld.value(forKey: "image") as? String)!
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

