//
//  FBTableViewCell.swift
//  customtbvcell
//
//  Created by Apple on 03/09/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class FBTableViewCell: UITableViewCell {
    @IBOutlet weak var profileimg: UIImageView!
    
    @IBOutlet weak var lbl: UILabel!

    @IBOutlet weak var like: UIButton!
    
    @IBOutlet weak var comment: UIButton!
    @IBOutlet weak var proimg: UIImageView!
    
    @IBOutlet weak var post: UIButton!
    @IBOutlet weak var statuslbl: UILabel!
    
    @IBOutlet weak var share: UIButton!
    
    @IBOutlet weak var typhere: UITextField!
    
    @IBAction func cmt(_ sender: Any) {
        typhere.isHidden = false
        post.isHidden = false
    }
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
