//
//  ViewController.swift
//  MapDemo
//
//  Created by Welborn Machado on 08/10/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    let locationmanger = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mapView.delegate = self

        let ann = MKPointAnnotation()
        ann.title = "My location"
        ann.coordinate = CLLocationCoordinate2DMake(18.2564, 75.5645)
        mapView.addAnnotation(ann)
        mapView.mapType = .hybridFlyover
        
        mapView.showsUserLocation = true
        
       // let annregion = MKCoordinateRegionMake(CLLocationCoordinate2DMake(18.2564, 75.5645), MKCoordinateSpanMake(20, 20))
       // mapView.setRegion(annregion, animated: true)
        
        mapView.setUserTrackingMode(.followWithHeading, animated: true)
        
        print(mapView.isUserLocationVisible)
        
        locationmanger.delegate = self
        locationmanger.desiredAccuracy = kCLLocationAccuracyBest
        locationmanger.requestWhenInUseAuthorization()
        locationmanger.startUpdatingLocation()
        
    }

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        print(locations)
    }
    
    public func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation){
        print("User location",userLocation.coordinate.latitude)
    }
    
    public func mapView(_ mapView: MKMapView, didFailToLocateUserWithError error: Error){
        print("Fail to update",error.localizedDescription)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

