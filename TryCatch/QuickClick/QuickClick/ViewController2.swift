//
//  ViewController2.swift
//  QuickClick
//
//  Created by Welborn Machado on 19/08/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //incase you are not checking on storyboard hide when stopped
        actInd.hidesWhenStopped = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @available(iOS 2.0, *)
     public func webViewDidStartLoad(_ webView: UIWebView){
        print("webViewDidStartLoad");
    }
    
    @available(iOS 2.0, *)
     public func webViewDidFinishLoad(_ webView: UIWebView){
        print("webViewDidFinishLoad");
        actInd.stopAnimating()

    }
    
    @available(iOS 2.0, *)
     public func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        print("didFailLoadWithError",error.localizedDescription);
        actInd.stopAnimating()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
