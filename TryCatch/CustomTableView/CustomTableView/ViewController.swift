//
//  ViewController.swift
//  CustomTableView
//
//  Created by Welborn Machado on 03/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var fbTableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let filePath = Bundle.main.path(forResource: "Info", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: filePath!)
        let savedDict = dict?.value(forKey: "MyDict") as! NSDictionary
        
        let arr = savedDict.allKeys as NSArray
        
        
        print(savedDict.value(forKey: arr[1] as! String))
        
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 10
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : fbTVC = tableView.dequeueReusableCell(withIdentifier: "fbTVC") as! fbTVC
        cell.myLbl.text = "This is cell"
        return cell
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

