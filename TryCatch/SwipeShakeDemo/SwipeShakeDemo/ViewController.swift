//
//  ViewController.swift
//  SwipeShakeDemo
//
//  Created by Welborn Machado on 08/10/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(leftCalled))
        leftSwipe.direction = .left
        self.view.addGestureRecognizer(leftSwipe)
        
        
    }

    func leftCalled(){
        print("left swipe")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if event?.subtype == UIEventSubtype.motionShake{
            print("shake")
        }
    }
}

