//
//  ViewController.swift
//  ImagePicker
//
//  Created by Welborn Machado on 16/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var imgV: UIImageView!
    
    @IBAction func gallPress(_ sender: Any) {
        let imgPicker = UIImagePickerController()
        imgPicker.sourceType = .photoLibrary
        imgPicker.delegate = self
        //to get selected portion
        imgPicker.allowsEditing = true
        present(imgPicker, animated: true, completion: nil)
        
    }
    
   public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
   {
        print(info)
//    let img = info[UIImagePickerControllerOriginalImage] as! UIImage
    
    //if you keep editing true
    let img = info[UIImagePickerControllerEditedImage] as! UIImage
    
    imgV.image = img
    dismiss(animated: true, completion: nil)

    
    }
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        print("Cancel press")
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

