//
//  ViewController.swift
//  Viewanimation
//
//  Created by Welborn Machado on 16/09/17.
//  Copyright © 2017 Welborn Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imgV: UIImageView!
    var imgW :CGFloat = 0
    var imgH:CGFloat = 0
    
    @IBAction func fadeInPress(_ sender: Any) {
        
        UIView.animate(withDuration: 5.0, animations: { 
            self.imgV.alpha = 1
        }) { (Bool) in
            self.imgV.frame = CGRect(x: self.imgV.frame.origin.x, y: self.imgV.frame.origin.y, width: 0, height: 0)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        imgV.alpha = 0
        imgW = self.imgV.frame.size.width
        imgH = self.imgV.frame.size.height

        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func growPress(_ sender: Any) {
        
        UIView.animate(withDuration: 5.0, animations: {
            self.imgV.frame = CGRect(x: self.imgV.frame.origin.x, y: self.imgV.frame.origin.y, width: self.imgW, height: self.imgH)
            
        }) { (Bool) in
            
            self.imgV.frame = CGRect(x: self.imgV.frame.origin.x-500, y: self.imgV.frame.origin.y, width: self.imgW, height: self.imgH)

        }

        
    }
    @IBAction func slidePress(_ sender: Any) {
        UIView.animate(withDuration: 5.0, animations: {
            self.imgV.frame = CGRect(x: self.imgV.frame.origin.x+500, y: self.imgV.frame.origin.y, width: self.imgW, height: self.imgH)
            
        }) { (Bool) in
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

